package rockPaperScissors;

//import java.text.BreakIterator;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    

    //############################################################################################

    public void run() {
        while (true) {
            // The random section for computer
            String[] rps = {"rock", "paper","scissors"};
            String computerMove = rps[new Random().nextInt(rps.length)];
            System.out.println("Let's play round" + " " + roundCounter);
            String humanchoice;
            // Valid answer:
            while (true) {
                humanchoice = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
                if (rpsChoices.contains(humanchoice)){
                    break;
                    } 
                    System.out.println("I do not understand"+ " " + humanchoice + ". " + "Could you try again?");
                }
        // if it's a tie
        if (humanchoice.equals(computerMove)){
            System.out.println("Human chose" + " " + humanchoice + ", computer chose" + " " + computerMove + ". " + "It's a tie!");
            System.out.println("Score:"+ " " + "human" + " " + humanScore + ", " + "computer" + " " + computerScore);
            roundCounter++;
        }

        // Who wins:
        else if (humanchoice.equals("rock") && (computerMove.equals("paper"))){
            roundCounter++;
            computerScore++;
            System.out.println("Human chose" + " " + humanchoice + ", computer chose" + " " + computerMove + ". " + "Computer Wins!");
            System.out.println("Score:"+ " " + "human" + " " + humanScore + ", " + "computer" + " " + computerScore);
        
        }
        else if (humanchoice.equals("paper") && (computerMove.equals("scissors"))){
            roundCounter++;
            computerScore++;
            System.out.println("Human chose" + " " + humanchoice + ", computer chose" + " " + computerMove + ". " + "Computer Wins!");
            System.out.println("Score:"+ " " + "human" + " " + humanScore + ", " + "computer" + " " + computerScore);
        } 

        else if (humanchoice.equals("scissors") && (computerMove.equals("rock"))){
            roundCounter++;
            computerScore++;
            System.out.println("Human chose" + " " + humanchoice + ", computer chose" + " " + computerMove + ". " + "Computer Wins!");
            System.out.println("Score:"+ " " + "human" + " " + humanScore + ", " + "computer" + " " + computerScore);
        } 

        else {
            roundCounter++;
            humanScore++;
            System.out.println("Human chose" + " " + humanchoice + ", computer chose" + " " + computerMove + ". " + "Human Wins!");
            System.out.println("Score:"+ " " + "human" + " " + humanScore + ", " + "computer" + " " + computerScore);
        }
            
        String continueAgain = readInput("Do you wish to continue playing? (y/n)?");
        if (!continueAgain.equals("y")) {
            System.out.println("Bye bye :)");
            break;
            } 
        }
    }

        /**
         * Reads input from console with given prompt
         * @param prompt
         * @return string input answer from user
         */

            public String readInput(String prompt) {
                System.out.println(prompt);
                String userInput = sc.next();
                return userInput;
            }
        }
